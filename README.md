# Docker Images

Modular solutions built as Alpine-based Docker images

<table>
  <thead>
    <tr>
      <th align="left">Images</th>
      <th align="left">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td valign="top">
        <a
          href="https://github.com/chiefmikey/docker-images/tree/main/alpine-inject"
          target="_blank"
          >Alpine Inject</a
        >
      </td>
      <td valign="top">Externally inject shell commands</td>
    </tr>
    <tr>
      <td valign="top">
        <a
          href="https://github.com/chiefmikey/docker-images/tree/main/alpine-cassandra"
          target="_blank"
          >Alpine Cassandra</a
        >
      </td>
      <td valign="top">Initialize an Apache Cassandra database</td>
    </tr>
    <tr>
      <td valign="top">
        <a
          href="https://github.com/chiefmikey/docker-images/tree/main/alpine-api"
          target="_blank"
          >Alpine API</a
        >
      </td>
      <td valign="top">API template served with Koa routing to MongoDB</td>
    </tr>
    <tr>
      <td valign="top">
        <a
          href="https://github.com/chiefmikey/docker-images/tree/main/alpine-craftcms"
          target="_blank"
          >Alpine CraftCMS</a
        >
      </td>
      <td valign="top">Serve a new CraftCMS installation</td>
    </tr>
  </tbody>
</table>
